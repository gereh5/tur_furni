from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from product.models import Brand, Category
from product.forms import ProductForm


def categories():
    category_list = {}

    for place in Category.objects.values('name'):
        category_list[place['name']] = place['name']
    return category_list


def paginator(request, product_list):
    page = request.GET.get('page', 1)
    paginator = Paginator(product_list, 12)
    try:
        products = paginator.page(page)
    except PageNotAnInteger:
        products = paginator.page(1)
    except EmptyPage:
        products = paginator.page(paginator.num_pages)
    return products

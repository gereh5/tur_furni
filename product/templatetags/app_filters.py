from django import template

register = template.Library()


@register.filter(name='custom_split')
def custom_split(value):
    return value.name.lower().replace(' ', '-')


@register.filter(name='get_query_string')
def get_query_string(value):
    return value.GET.get('q').upper()

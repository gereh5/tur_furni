from product.models import Category, Brand


def add_variable_to_context(request):
    category_list = list(Category.objects.values_list('name', flat=True).order_by('name'))
    brand_list = list(Brand.objects.values_list('name', flat=True).order_by('name'))

    return {
        'product_categories': category_list,
        'product_brands': brand_list
    }

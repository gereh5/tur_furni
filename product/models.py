from ckeditor_uploader.fields import RichTextUploadingField
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db import models


class Brand(models.Model):
    name = models.CharField(max_length=20)

    def __str__(self):
        return self.name


class Category(models.Model):
    name = models.CharField(max_length=20)
    image = models.ImageField(upload_to='static/uploads/category_image', null=True)

    def __str__(self):
        return self.name


class SubCategory(models.Model):
    name = models.CharField(max_length=20)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='subcategories')

    def __str__(self):
        return self.name


class Products(models.Model):
    name = models.CharField(max_length=20)
    code = models.CharField(max_length=20)
    dimension = models.CharField(max_length=50)
    features = models.CharField(max_length=255)
    material = models.CharField(max_length=50)
    color = models.CharField(max_length=50)
    description = models.TextField()
    date_added = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)
    new = models.BooleanField(default=False)
    home_carousel = models.BooleanField(default=False)
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True)
    subcategory = models.ForeignKey(SubCategory, on_delete=models.SET_NULL, null=True)
    brand = models.ForeignKey(Brand, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.name


class Image(models.Model):
    product = models.ForeignKey(Products, on_delete=models.CASCADE, related_name='product_image')
    image = models.ImageField(upload_to='static/uploads/products', null=True, blank=True)
    date_added = models.DateTimeField(auto_now_add=True)
    order = models.IntegerField()

    class Meta:
        ordering = ['order']


class Stories(models.Model):
    title = models.CharField(max_length=100)
    snippet = models.TextField()
    body = RichTextUploadingField()
    story_date = models.DateField()
    date_added = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)
    title_image = models.ImageField(upload_to='static/uploads/stories')
    author = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.title


class HomePage(models.Model):
    text = RichTextUploadingField()


class HomePageHighlight(models.Model):
    title = models.CharField(max_length=100)
    sub_title = models.CharField(max_length=100)
    list_product = models.CharField(max_length=255)


class Catalog(models.Model):
    def validate_file_extension(value):
        if value.file.content_type != 'application/pdf':
            raise ValidationError(u'Please insert PDF file')

    title = models.CharField(max_length=100)
    file = models.FileField(upload_to='static/uploads/catalog',
                            validators=[validate_file_extension])


class AboutCompany(models.Model):
    image_header = models.ImageField(upload_to='static/uploads/about_company_header')
    text = RichTextUploadingField()


class AboutCommitment(models.Model):
    image_header = models.ImageField(upload_to='static/uploads/about_commitment_header')
    text = RichTextUploadingField()
    image_footer = models.ImageField(upload_to='static/uploads/about_commitment_footer')
    footer_text = models.TextField()


class AboutDesigner(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField()
    image = models.ImageField(upload_to='static/uploads/designers')

from django.contrib.auth.decorators import login_required, user_passes_test
from django.forms import formset_factory
from django.http import JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages

from product.forms import ImageForm
from product.models import Products, SubCategory, Category, Image
from ..forms import ProductForm
from ..views_helper import paginator


def product_index(request, category=None, subcategory=None):
    if category:
        if category.lower() == 'detail':
            return product_detail(request, subcategory)
        elif category.lower() == 'new':
            return newest_products(request)
        elif category.lower() == 'brand':
            return product_brand(request, subcategory)
        elif subcategory:
            sub_cat = subcategory.replace('-', ' ').title()
            product_list = Products.objects.filter(is_active=True, subcategory__name__iexact=sub_cat).order_by(
                '-date_added')

            selected_subcategory = SubCategory.objects.get(name=sub_cat)
            list_subcategory = SubCategory.objects.filter(category_id=selected_subcategory.category)

            products = paginator(request, product_list)
            return render(request, 'product_subcategory_index.html',
                          {'nav': 'product', 'product_list': products, 'category_name': category.lower(),
                           'subcategories': list_subcategory})
        else:
            cat_name = category.capitalize()
            product_list = Products.objects.filter(is_active=True, category__name__iexact=cat_name)\
                .order_by('-date_added')
            subcategory = SubCategory.objects.filter(category__name=cat_name)
            if not subcategory:
                subcategory = []

            products = paginator(request, product_list)
            return render(request, 'product_category_index.html',
                          {'nav': 'product', 'product_list': products, 'category_name': category.lower(),
                           'subcategories': subcategory})
    else:
        categories = Category.objects.all().order_by('name')
        return render(request, 'category_index.html', {'nav': 'category_index', 'categories': categories})


def product_detail(request, id):
    product = get_object_or_404(Products.objects.filter(is_active=True), pk=id)
    return render(request, 'product_detail.html', {'nav': 'product', 'product': product})


def newest_products(request):
    products = Products.objects.filter(new=True)
    return render(request, 'product_subcategory_index.html',
                  {'nav': 'product', 'category_name': 'new', 'product_list': products})


def product_brand(request, brand):
    product = Products.objects.filter(brand__name__iexact=brand.capitalize()).order_by('date_added')
    return render(request, 'product_subcategory_index.html', {'category_name': brand.lower(), 'product_list': product})


@login_required
@user_passes_test(lambda u: u.is_superuser)
def edit_product(request, id=None):
    image_formset = formset_factory(ImageForm, can_delete=True)

    if id is not None:
        product_instance = get_object_or_404(Products, pk=id)
        images = Image.objects.filter(product_id=id).order_by('order')
        image_instances = [dict(image=instance.image, order=instance.order) for instance in images]
        msg = 'Successfully edit product'
        title = 'Edit'
    else:
        product_instance = Products()
        images = Image.objects.none()
        image_instances = Image.objects.none()
        msg = 'Successfully add product'
        title = 'Add New'

    if request.method == 'POST':
        product_form = ProductForm(request.POST, request.FILES, instance=product_instance)
        formset = image_formset(request.POST, request.FILES, initial=image_instances)
        if product_form.is_valid() and formset.is_valid():
            product = product_form.save(commit=False)
            product.save()

            images.delete()
            for form in formset:
                if form.cleaned_data != {}:
                    image = form.save(commit=False)
                    image.product = product
                    if image.image:
                        Image.objects.filter(product=product.id, image=image.image).delete()
                        image.save()

            messages.success(request, msg)
            return redirect('cms_product')
    else:
        product_form = ProductForm(instance=product_instance)
        formset = image_formset(initial=image_instances)
    return render(request, 'cms/add_product.html', {'form': product_form, 'formset': formset, 'title': title})


@login_required
@user_passes_test(lambda u: u.is_superuser)
def product_activate(request, id, status):
    try:
        if status.lower() == 'true':
            Products.objects.filter(id=id).update(is_active=True)
        else:
            Products.objects.filter(id=id).update(is_active=False)
    except Exception as e:
        return JsonResponse({'message': str(e)})
    return JsonResponse({'message': 'success'})


@login_required
@user_passes_test(lambda u: u.is_superuser)
def product_new(request, id, status):
    try:
        if status.lower() == 'true':
            Products.objects.filter(id=id).update(new=True)
        else:
            Products.objects.filter(id=id).update(new=False)
    except Exception as e:
        return JsonResponse({'message': str(e)})
    return JsonResponse({'message': 'success'})

from django.contrib.auth.decorators import login_required, user_passes_test
from django.shortcuts import render
from django.db.models import Q

from product.models import Products, Stories
from product.views_helper import paginator


def findus(request):
    return render(request, 'findus.html', {'nav': 'about'})


def search(request):
    keyword = request.GET['q']
    active_product = Products.objects.filter(is_active=True)
    product_list = active_product.filter(
        Q(name__icontains=keyword) |
        Q(description__icontains=keyword) |
        Q(category__name__icontains=keyword) |
        Q(brand__name__icontains=keyword)).order_by('-date_added')

    products = paginator(request, product_list)
    return render(request, 'product_subcategory_index.html',
                  {'nav': 'product', 'product_list': products})


@login_required
@user_passes_test(lambda u: u.is_superuser)
def cms_page(request):
    return render(request, 'cms_page.html')


@login_required
@user_passes_test(lambda u: u.is_superuser)
def cms_product(request):
    product_list = Products.objects.all().order_by('id')
    return render(request, 'cms/product.html', {'nav': 'product', 'products': product_list})


@login_required
@user_passes_test(lambda u: u.is_superuser)
def cms_story(request):
    story_list = Stories.objects.all().order_by('id')
    return render(request, 'cms/story.html', {'nav': 'story', 'stories': story_list})

from django.contrib.auth.decorators import login_required, user_passes_test
from django.http import JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages

from product.models import Stories
from ..forms import StoriesForm
from ..views_helper import paginator


def story_index(request):
    story_list = Stories.objects.filter(is_active=True).order_by('-date_added')

    stories = paginator(request, story_list)
    return render(request, 'story_index.html', {'nav': 'stories', 'story_list': stories})


def story_detail(request, id):
    story = get_object_or_404(Stories.objects.filter(is_active=True), pk=id)
    return render(request, 'story_detail.html', {'nav': 'stories', 'story': story})


@login_required
@user_passes_test(lambda u: u.is_superuser)
def edit_story(request, id=None):
    if id is not None:
        story_instance = get_object_or_404(Stories, pk=id)
        msg = 'Successfully edit story'
        title = 'Edit'
    else:
        story_instance = Stories()
        msg = 'Successfully add story'
        title = 'Add New'

    if request.method == 'POST':
        form = StoriesForm(request.POST, request.FILES, instance=story_instance)
        if form.is_valid():
            story_form = form.save(commit=False)
            story_form.author = request.user
            story_form.save()
            messages.success(request, msg)
            return redirect('cms_story')

    else:
        form = StoriesForm(instance=story_instance)
    return render(request, 'cms/add_story.html', {'form': form, 'title': title})


@login_required
@user_passes_test(lambda u: u.is_superuser)
def story_activate(request, id, status):
    try:
        if status.lower() == 'true':
            Stories.objects.filter(id=id).update(is_active=True)
        else:
            Stories.objects.filter(id=id).update(is_active=False)
    except Exception as e:
        return JsonResponse({'message': str(e)})
    return JsonResponse({'message': 'success'})

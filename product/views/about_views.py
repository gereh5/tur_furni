from django.contrib import messages
from django.contrib.auth.decorators import login_required, user_passes_test
from django.shortcuts import render, redirect, get_object_or_404

from product.forms import AboutCompanyForm, AboutCommitmentForm, AboutDesignerForm
from product.models import AboutCompany, AboutCommitment, AboutDesigner


def about_company(request):
    company = AboutCompany.objects.first()
    return render(request, 'about_company_index.html', {'company': company})


def about_commitment(request):
    commitment = AboutCommitment.objects.first()
    return render(request, 'about_commitment_index.html', {'commitment': commitment})


def about_designer(request):
    designers = AboutDesigner.objects.all()
    return render(request, 'about_designer_index.html', {'designers': designers})


# --------------------------------------- THIS CODE BELOW IS FOR CMS -----------------------------------------

@login_required
@user_passes_test(lambda u: u.is_superuser)
def about_company_edit(request):
    about_company_body = AboutCompany.objects.first()
    if request.method == 'POST':
        about_company_form = AboutCompanyForm(request.POST, request.FILES, instance=about_company_body)
        if about_company_form.is_valid():
            about_company_form.save()
            messages.success(request, 'Successfully edit about company page')
            return redirect('edit_about_company')
    else:
        about_company_form = AboutCompanyForm(instance=about_company_body)
    return render(request, 'cms/about_company.html', {'nav': 'home', 'form': about_company_form})


@login_required
@user_passes_test(lambda u: u.is_superuser)
def about_commitment_edit(request):
    about_commitment_body = AboutCommitment.objects.first()
    if request.method == 'POST':
        about_commitment_form = AboutCommitmentForm(request.POST, request.FILES, instance=about_commitment_body)
        if about_commitment_form.is_valid():
            about_commitment_form.save()
            messages.success(request, 'Successfully edit about commitment page')
            return redirect('edit_about_commitment')
    else:
        about_commitment_form = AboutCommitmentForm(instance=about_commitment_body)
    return render(request, 'cms/about_commitment.html', {'nav': 'home', 'form': about_commitment_form})


@login_required
@user_passes_test(lambda u: u.is_superuser)
def cms_designer_list(request):
    designer_list = AboutDesigner.objects.all().order_by('id')
    return render(request, 'cms/about_designer.html', {'nav': 'home', 'designers': designer_list})


@login_required
@user_passes_test(lambda u: u.is_superuser)
def about_designer_edit(request, id=None):
    if id is not None:
        designer_instance = get_object_or_404(AboutDesigner, pk=id)
        msg = 'Successfully edit designer'
        title = 'Edit'
    else:
        designer_instance = AboutDesigner()
        msg = 'Successfully add designer'
        title = 'Add New'

    if request.method == 'POST':
        designer_form = AboutDesignerForm(request.POST, request.FILES, instance=designer_instance)
        if designer_form.is_valid():
            designer_form.save()
            messages.success(request, msg)
            return redirect('cms_designer_list')
    else:
        designer_form = AboutDesignerForm(instance=designer_instance)
    return render(request, 'cms/add_designer.html', {'nav': 'home', 'form': designer_form, 'title': title})


@login_required
@user_passes_test(lambda u: u.is_superuser)
def delete_designer(request, id):
    AboutDesigner.objects.get(id=id).delete()
    messages.success(request, 'Successfully delete designer')
    return redirect('cms_designer_list')

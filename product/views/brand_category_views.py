from django.contrib.auth.decorators import login_required, user_passes_test
from django.shortcuts import redirect, render, get_object_or_404
from django.contrib import messages

from product.models import Brand, SubCategory, Category
from ..forms import BrandForm, CategoryForm, SubcategoryForm


@login_required
@user_passes_test(lambda u: u.is_superuser)
def brand_index(request, id=None):
    brand = Brand.objects.all().order_by('name')

    if id is not None:
        brand_instance = Brand.objects.get(id=id)
        msg = 'Successfully edit brand'
        title = 'Edit'
    else:
        brand_instance = Brand()
        msg = 'Successfully add brand'
        title = 'Add new'

    if request.method == 'POST':
        form = BrandForm(request.POST, instance=brand_instance)
        if form.is_valid():
            form.save()
            messages.success(request, msg)
            return redirect('brand_index')
    else:
        form = BrandForm(instance=brand_instance)

    return render(request, 'cms/brand_page.html', {'brands': brand, 'form': form, 'title': title})


@login_required
@user_passes_test(lambda u: u.is_superuser)
def add_category(request, id=None):
    category = Category.objects.all().order_by('name')

    if id is not None:
        category_instance = Category.objects.get(id=id)
        msg = 'Successfully edit category'
        title = 'Edit'
    else:
        category_instance = Category()
        msg = 'Successfully add category'
        title = 'Add new'

    if request.method == 'POST':
        form = CategoryForm(request.POST, request.FILES, instance=category_instance)
        if form.is_valid():
            form.save()
            messages.success(request, msg)
            return redirect('category_index')
    else:
        form = CategoryForm(instance=category_instance)

    return render(request, 'cms/add_category.html', {'categories': category, 'form': form, 'title': title})


@login_required
@user_passes_test(lambda u: u.is_superuser)
def add_subcategory(request, id=None):
    categories = []
    list_category = Category.objects.all().order_by('name')
    for category in list_category:
        list_subcategory = SubCategory.objects.filter(category_id=category.id).order_by('name')
        if not list_subcategory:
            categories.append(dict(category=category.name, subcategory=''))
        for subcategory in list_subcategory:
            categories.append(dict(id=subcategory.id, category=category.name, subcategory=subcategory.name))

    if id is not None:
        subcategory_instance = SubCategory.objects.get(id=id)
        msg = 'Successfully edit subcategory'
        title = 'Edit'
    else:
        subcategory_instance = SubCategory()
        msg = 'Successfully add subcategory'
        title = 'Add new'

    if request.method == 'POST':
        subcategory_form = SubcategoryForm(request.POST, instance=subcategory_instance)
        if subcategory_form.is_valid():
            subcategory_form.save()
            messages.success(request, msg)
            return redirect('subcategory_index')
    else:
        subcategory_form = SubcategoryForm(instance=subcategory_instance)

    return render(request, 'cms/add_subcategory.html',
                  {'subcategories': categories,
                   'subcategory_form': subcategory_form,
                   'title': title})


@login_required
@user_passes_test(lambda u: u.is_superuser)
def edit_subcategory(request, id):
    subcategory_instance = get_object_or_404(SubCategory, pk=id)

    if request.method == 'POST':
        subcategory_form = SubcategoryForm(request.POST, instance=subcategory_instance)
        if subcategory_form.is_valid():
            subcategory_form.save()
            messages.success(request, 'Successfully edit subcategory')
            return redirect('subcategory_index')
    else:
        subcategory_form = SubcategoryForm(instance=subcategory_instance)

    return render(request, 'cms/edit_subcategory.html',
                  {'subcategory_form': subcategory_form})


def load_subcategory(request):
    category_id = request.GET.get('category')
    subcategory = SubCategory.objects.filter(category_id=category_id).order_by('name')
    return render(request, 'cms/subcategory_dropdown_list.html', {'subcategories': subcategory})

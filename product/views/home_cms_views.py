import ast
from django.contrib.auth.decorators import login_required, user_passes_test
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render, redirect, get_object_or_404
from django.http import JsonResponse
from django.contrib import messages

from product.models import Products, HomePage, HomePageHighlight
from product.forms import HomeBodyForm, HomeHighlightForm


def home(request):
    product_list = []
    products = Products.objects.filter(home_carousel=True)
    for product in products:
        try:
            image = product.product_image.get(order=3)
            product_list.append(dict(id=product.id, image=image.image))
        except ObjectDoesNotExist:
            image = []
            product_list.append(dict(id=product.id, image=image))

    home_body = HomePage.objects.first()
    home_page_highlight = HomePageHighlight.objects.all()

    highlight_list = []
    for highlight in home_page_highlight:
        highlight_product = []
        for product in ast.literal_eval(highlight.list_product):
            prod = Products.objects.get(id=product)
            highlight_product.append(dict(id=product, url=prod.image.url))
        highlight_list.append(dict(title=highlight.title,
                                   subtitle=highlight.sub_title,
                                   product=highlight_product))

    return render(request, 'home.html', {'nav': 'home', 'product_list': product_list,
                                         'home_body': home_body, 'highlights': highlight_list})


# ---------------------------------- THIS PART BELOW IS FOR CMS ---------------------------------------

@login_required
@user_passes_test(lambda u: u.is_superuser)
def home_image_carousel(request):
    product_list = Products.objects.all().order_by('id')
    return render(request, 'cms/home_image_carousel.html', {'nav': 'home', 'products': product_list})


@login_required
@user_passes_test(lambda u: u.is_superuser)
def home_body(request):
    home_body = HomePage.objects.first()
    if request.method == 'POST':
        home_body_form = HomeBodyForm(request.POST, request.FILES, instance=home_body)
        if home_body_form.is_valid():
            home_body_form.save()
            messages.success(request, 'Successfully edit home body')
            return redirect('home_body')
    else:
        home_body_form = HomeBodyForm(instance=home_body)
    return render(request, 'cms/home_body.html', {'nav': 'home', 'form': home_body_form})


@login_required
@user_passes_test(lambda u: u.is_superuser)
def home_list_highlight(request):
    highlight_list = HomePageHighlight.objects.all().order_by('id')
    return render(request, 'cms/home_highlight.html', {'nav': 'home', 'highlights': highlight_list})


@login_required
@user_passes_test(lambda u: u.is_superuser)
def home_edit_highlight(request, id=None):
    if id is not None:
        highlight_instance = get_object_or_404(HomePageHighlight, pk=id)
        msg = 'Successfully edit highlight'
        title = 'Edit'
    else:
        highlight_instance = HomePageHighlight()
        msg = 'Successfully add highlight'
        title = 'Add New'

    if request.method == 'POST':
        home_highlight_form = HomeHighlightForm(request.POST, instance=highlight_instance)
        if home_highlight_form.is_valid():
            form = home_highlight_form.save(commit=False)

            list_product = home_highlight_form.cleaned_data['list_product']
            list_product_str = save_product_list(list_product)

            form.list_product = list_product_str
            form.save()
            messages.success(request, msg)
            return redirect('home_list_highlights')
    else:
        home_highlight_form = HomeHighlightForm(instance=highlight_instance)
    return render(request, 'cms/home_add_highlight.html', {'nav': 'home', 'form': home_highlight_form, 'title': title})


def save_product_list(products):
    list_product_id = [product.id for product in products]
    return str(list_product_id)


@login_required
@user_passes_test(lambda u: u.is_superuser)
def delete_highlight(request, id):
    HomePageHighlight.objects.get(id=id).delete()
    messages.success(request, 'Successfully delete highlight')
    return redirect('home_list_highlights')


@login_required
@user_passes_test(lambda u: u.is_superuser)
def status_image_carousel(request, id, status):
    try:
        if status.lower() == 'true':
            Products.objects.filter(id=id).update(home_carousel=True)
        else:
            Products.objects.filter(id=id).update(home_carousel=False)
    except Exception as e:
        return JsonResponse({'message': str(e)})
    return JsonResponse({'message': 'success'})

import os
from django.contrib import messages
from django.core.files.storage import FileSystemStorage
from django.http import HttpResponse, HttpResponseNotFound
from django.shortcuts import get_object_or_404, redirect, render

from product.forms import CatalogForm
from product.models import Catalog


def add_catalog(request, id=None):
    catalogs = Catalog.objects.all().order_by('id')

    if id is not None:
        catalog_instance = get_object_or_404(Catalog, pk=id)
        msg = 'successfully edit catalog'
        title = 'Edit'
    else:
        catalog_instance = Catalog()
        msg = 'successfully add catalog'
        title = 'Add new'

    if request.method == 'POST':
        catalog_form = CatalogForm(request.POST, request.FILES, instance=catalog_instance)
        if catalog_form.is_valid():
            catalog_form.save()
            messages.success(request, msg)
            return redirect('add_catalog')
    else:
        catalog_form = CatalogForm(instance=catalog_instance)

    return render(request, 'cms/add_catalog.html', {'catalogs': catalogs, 'form': catalog_form, 'title': title})


def delete_catalog(request, id):
    Catalog.objects.get(id=id).delete()
    messages.success(request, 'Successfully delete catalog')
    return redirect('add_catalog')


def pdf_view(request, title=''):
    fs = FileSystemStorage(location='media/static/uploads/catalog')
    catalog = get_object_or_404(Catalog, title=title)

    filename = os.path.basename(catalog.file.url)
    if fs.exists(filename):
        with fs.open(filename) as pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            response['Content-Disposition'] = f'inline; filename="{filename}"'
            return response
    else:
        return HttpResponseNotFound('The requested pdf was not found in our server.')

from django import forms

from product.models import SubCategory, Category, HomePage, HomePageHighlight, Catalog, AboutCompany, AboutCommitment, \
    AboutDesigner, Image
from .models import Products, Brand, Stories


class ProductForm(forms.ModelForm):
    class Meta:
        model = Products
        fields = '__all__'
        exclude = ['new', 'home_carousel']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['subcategory'].queryset = SubCategory.objects.none()
        self.fields['subcategory'].required = False

        if 'category' in self.data:
            try:
                category_id = int(self.data.get('category'))
                self.fields['subcategory'].queryset = SubCategory.objects.filter(category_id=category_id).order_by(
                    'name')
            except (ValueError, TypeError):
                pass
        elif self.instance.pk:
            try:
                self.fields['subcategory'].queryset = self.instance.category.subcategories.order_by('name')
            except AttributeError:
                pass


class ImageForm(forms.ModelForm):
    class Meta:
        model = Image
        fields = ['image', 'order']


class StoriesForm(forms.ModelForm):
    class Meta:
        model = Stories
        fields = '__all__'
        exclude = ['author']


class BrandForm(forms.ModelForm):
    class Meta:
        model = Brand
        fields = '__all__'


class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = '__all__'


class SubcategoryForm(forms.ModelForm):
    class Meta:
        model = SubCategory
        fields = '__all__'


class HomeBodyForm(forms.ModelForm):
    class Meta:
        model = HomePage
        fields = '__all__'


class HomeHighlightForm(forms.ModelForm):
    list_product = forms.ModelMultipleChoiceField(queryset=Products.objects.all(),
                                                  widget=forms.CheckboxSelectMultiple(),
                                                  required=True)

    class Meta:
        model = HomePageHighlight
        fields = '__all__'


class CatalogForm(forms.ModelForm):
    class Meta:
        model = Catalog
        fields = '__all__'


class AboutCompanyForm(forms.ModelForm):
    class Meta:
        model = AboutCompany
        fields = '__all__'


class AboutCommitmentForm(forms.ModelForm):
    class Meta:
        model = AboutCommitment
        fields = '__all__'


class AboutDesignerForm(forms.ModelForm):
    class Meta:
        model = AboutDesigner
        fields = '__all__'

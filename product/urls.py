from django.urls import path
from .views.main_views import search, findus, cms_page, cms_product, cms_story
from .views.product_views import product_detail, edit_product, \
    product_activate, product_new, product_index
from .views.story_views import story_index, story_detail, edit_story, story_activate
from .views.brand_category_views import brand_index, load_subcategory, add_subcategory, \
    add_category, edit_subcategory
from .views.home_cms_views import home, home_image_carousel, home_body, home_list_highlight, \
    home_edit_highlight, delete_highlight, status_image_carousel
from .views.catalog_views import add_catalog, delete_catalog, pdf_view
from .views.about_views import about_company_edit, about_commitment_edit, about_designer_edit, \
    cms_designer_list, delete_designer, about_company, about_commitment, about_designer

urlpatterns = [
    path('', home, name='home'),
    path('search/', search, name='search'),
    path('products/', product_index, name='product_index'),
    path('products/<str:category>', product_index, name='product_category'),
    path('products/<str:category>/<str:subcategory>', product_index, name='product_subcategory'),
    path('about_us/', findus, name='about_us'),
    path('our_company/', about_company, name='about_company'),
    path('our_commitment/', about_commitment, name='about_commitment'),
    path('our_designer/', about_designer, name='about_designer'),
    path('cms/', cms_page, name='cms_page'),
    path('cms/product/', cms_product, name='cms_product'),
    path('cms/story/', cms_story, name='cms_story'),

    path('add_product/', edit_product, name='add_product'),
    path('products/detail/<int:id>', product_detail, name='product_detail'),
    path('edit_product/<int:id>', edit_product, name='product_edit'),
    path('update_status_product/<int:id>/<str:status>', product_activate, name='product_activate'),
    path('update_new_product/<int:id>/<str:status>', product_new, name='product_new'),

    path('blog/', story_index, name='stories'),
    path('add_story/', edit_story, name='add_story'),
    path('story_detail/<int:id>', story_detail, name='story_detail'),
    path('edit_story/<int:id>', edit_story, name='story_edit'),
    path('update_status_story/<int:id>/<str:status>', story_activate, name='story_activate'),

    path('add_brands/', brand_index, name='brand_index'),
    path('edit_brands/<int:id>', brand_index, name='edit_brand'),
    path('add_category/', add_category, name='category_index'),
    path('edit_category/<int:id>', add_category, name='edit_category'),
    path('add_subcategory/', add_subcategory, name='subcategory_index'),
    path('edit_subcategory/<int:id>', edit_subcategory, name='edit_subcategory'),
    path('ajax/load-subcategory/', load_subcategory, name='ajax_load_subcategory'),

    path('cms/home/edit/image_carousel', home_image_carousel, name='home_image_carousel'),
    path('cms/home/edit/image_carousel/change_status/<int:id>/<str:status>', status_image_carousel,
         name='edit_status_image_carousel'),
    path('cms/home/edit/body', home_body, name='home_body'),
    path('cms/home/edit/list_highlight', home_list_highlight, name='home_list_highlights'),
    path('cms/home/edit/add_highlight', home_edit_highlight, name='add_highlights'),
    path('cms/home/edit/edit_highlight/<int:id>', home_edit_highlight, name='edit_highlights'),
    path('cms/home/edit/delete_highlight/<int:id>', delete_highlight, name='delete_highlights'),

    path('cms/about/edit/about_company', about_company_edit, name='edit_about_company'),
    path('cms/about/edit/about_commitment', about_commitment_edit, name='edit_about_commitment'),
    path('cms/about/designer_list', cms_designer_list, name='cms_designer_list'),
    path('cms/about/add_designer', about_designer_edit, name='add_designer'),
    path('cms/about/edit_designer/<int:id>', about_designer_edit, name='edit_designer'),
    path('cms/about/remove_designer/<int:id>', delete_designer, name='delete_designer'),

    path('cms/add/catalog/', add_catalog, name='add_catalog'),
    path('cms/edit/catalog/<int:id>', add_catalog, name='edit_catalog'),
    path('cms/delete/catalog/<int:id>', delete_catalog, name='delete_catalog'),
    path('show_pdf/<str:title>', pdf_view, name='pdf_view'),
]

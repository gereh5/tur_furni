# Furnitur Furnituran

### Prerequisites

Install postgres database, or if you use docker :

```
docker run --name {{container_name}} -e POSTGRES_PASSWORD=mysecretpassword -d -p 5432:5432 postgres
```

### Installing

Create python 3.6 environment, activate and install packages :

```
pip install -r requirements.txt
```

Create django superuser :

```
python manage.py createsuperuser
```

Migrate and make migrations :

```
python manage.py makemigrations
python manage.py migrate
```

Final step, insert data inside product_brand table (show brand choices)

### Acknowledgments

* User can add product after login

